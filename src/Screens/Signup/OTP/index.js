import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    StatusBar,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    Keyboard,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { verify_otp,resend_otp } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import FloatingLabelInput from "../../../components/FloatingLabelInput";
import LinearGradient from 'react-native-linear-gradient';

const userinfo = { name: 'admin', pass: 'admin', email: 'sh@gmail.com' };
const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
}

class OTP extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            passcode1: '',
            passcode2: '',
            passcode3: '',
            passcode4: '',
            error: null,
            timer: 5,
            done: "done",
            startDisable: false
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount() {
        console.log('otp', this.props.userdata);
    }

    componentWillUnmount() {
        clearInterval(this.clockCall);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Auth');
        return true;
    }

    componentDidMount() {
        this.clockCall = setInterval(() => {
            this.decrementClock();
        }, 1000);
    }

    decrementClock = () => {
        this.setState((prevstate) => ({ timer: prevstate.timer - 1 }));
    };

    _verify = async () => {
        var {
            passcode1,
            passcode2,
            passcode3,
            passcode4,
        } = this.state
        const data = {
            "email": this.props.userdata.email,
            "otp": passcode1 + passcode2 + passcode3 + passcode4
        }
        console.log(data.otp.length)
        if (data.otp.length === 4) {
            this.props.verify_otp(data, () => this.props.navigation.navigate('Auth'));
        }
        // alert(this.props.userdata.email)
    }

    _resendOTP = async () => {
        const data = {
            "email": this.props.userdata.email
        }
        this.props.resend_otp(data);
    }



    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: "transparent",
                justifyContent: "center"
            }}>
                <StatusBar hidden />
                <ImageBackground style={styles.imgBackground}
                    resizeMode='stretch'
                    source={require('../../../assets/Background.png')}>
                    <View style={{
                        flex: 1,
                        backgroundColor: "transparent",
                        justifyContent: "center"
                    }}>
                        <Image style={{ marginTop: verticalScale(79), marginLeft: scale(30), width: scale(198), height: scale(24) }}
                            resizeMode='cover'
                            source={require('../../../assets/VACCINELEDGER.png')} />
                        <Text style={{ marginLeft: scale(30), marginTop: verticalScale(8), fontSize: scale(26), color: '#000000', fontFamily: "Roboto-Regular" }}>
                            Welcome
                        </Text>

                        <View style={[{ marginLeft: scale(30), marginRight: scale(30), height: scale(263), borderRadius: 10, backgroundColor: "#FFFFFF", marginTop: verticalScale(64), justifyContent: "center", alignItems: "center" }, shadowStyle]}>
                            <Text style={{ marginTop: verticalScale(20), fontSize: scale(20), color: '#000000', fontFamily: "Roboto-Bold", fontWeight: "bold" }}>
                                Enter OTP
                         </Text>
                            <View style={{ marginTop: verticalScale(25), height: scale(50), flexDirection: "row" }}>
                                <TextInput style={{ width: scale(33), borderBottomWidth: 1, borderBottomColor: "#D6D6D6", alignItems: "center", textAlign: "center", fontSize: scale(20) }}
                                    maxLength={1}
                                    ref="input_1"
                                    keyboardType={'numeric'}
                                    //autoFocus={true}
                                    value={this.state.passcode1}
                                    onChangeText={passcode1 => {
                                        this.setState({ passcode1 })
                                        if (passcode1) this.refs.input_2.focus(); //assumption is TextInput ref is input_2
                                    }}
                                />

                                <TextInput style={{ marginLeft: scale(8), width: scale(33), borderBottomWidth: 1, borderBottomColor: "#D6D6D6", alignItems: "center", textAlign: "center", fontSize: scale(20), }}
                                    maxLength={1}
                                    ref="input_2"
                                    keyboardType={'numeric'}
                                    value={this.state.passcode2}
                                    onKeyPress={({ nativeEvent }) => {
                                        if (nativeEvent.key === 'Backspace') {
                                            this.refs.input_1.focus();
                                        }
                                    }}
                                    onChangeText={passcode2 => {
                                        this.setState({ passcode2 })
                                        if (passcode2) this.refs.input_3.focus();
                                    }} />
                                <TextInput style={{ marginLeft: scale(8), width: scale(33), borderBottomWidth: 1, borderBottomColor: "#D6D6D6", alignItems: "center", textAlign: "center", fontSize: scale(20) }}
                                    maxLength={1}
                                    ref="input_3"
                                    keyboardType={'numeric'}
                                    value={this.state.passcode3}
                                    onKeyPress={({ nativeEvent }) => {
                                        if (nativeEvent.key === 'Backspace') {
                                            this.refs.input_2.focus();
                                        }
                                    }}
                                    onChangeText={passcode3 => {
                                        this.setState({ passcode3 })
                                        if (passcode3) this.refs.input_4.focus(); //assumption is TextInput ref is input_2
                                    }} />
                                <TextInput style={{ marginLeft: scale(8), width: scale(33), borderBottomWidth: 1, borderBottomColor: "#D6D6D6", alignItems: "center", textAlign: "center", fontSize: scale(20) }}
                                    maxLength={1}
                                    ref="input_4"
                                    keyboardType={'numeric'}
                                    value={this.state.passcode4}
                                    onKeyPress={({ nativeEvent }) => {
                                        if (nativeEvent.key === 'Backspace') {
                                            this.refs.input_3.focus();
                                        }
                                    }}
                                    onChangeText={passcode4 => {
                                        this.setState({ passcode4 })
                                        if (passcode4) Keyboard.dismiss();
                                        //assumption is TextInput ref is input_2
                                    }} />
                            </View>

                            <TouchableOpacity style={{ marginTop: verticalScale(25), width: scale(100), height: scale(30),flexDirection:"row",alignItems:"center",justifyContent:"space-between", }}
                            onPress={()=>this._resendOTP()}>
                                <Image style={{width:scale(12),height:scale(12)}}
                    resizeMode='contain'
                    source={require('../../../assets/refresh.png')} />
                    <Text style={{ marginLeft: scale(7), fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#0B65C1", fontWeight: "bold" }}>Resend OTP</Text>

                            </TouchableOpacity>
                            
                            <TouchableOpacity
                                onPress={this._verify}
                                style={{ width: scale(133), height: scale(40), borderRadius: 8, backgroundColor: "red", justifyContent: "center", alignItems: "center", marginTop: verticalScale(25) }}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={['#0093E9', '#36C2CF']}
                                    style={{ width: scale(133), height: scale(40), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ fontSize: scale(20), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>VERIFY</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>

                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
        position: 'absolute',
    },
    wrapper: {
        flex: 1,
    },
})

function mapStateToProps(state) {
    return {
        otpdata: state.otp.otpdata,
        userdata: state.register.userdata
    }
}

export default connect(mapStateToProps, { verify_otp,resend_otp })(OTP)