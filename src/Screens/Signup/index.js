import React from 'react';
import { saveUser } from '../../Action'
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    StatusBar,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import FloatingLabelInput from "../../components/FloatingLabelInput";
import LinearGradient from 'react-native-linear-gradient';

const userinfo = { name: 'admin', pass: 'admin', email: 'sh@gmail.com' };
const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,

    elevation: 8,
}
class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            name: '',
            Email: '',
            pass: '',
            hidePassword: true,
            error: null,
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentDidMount() {
        console.log('signup', this.props.userdata);

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Auth');
        return true;
    }
    validateEmail() {
        let reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // /^([a-z0-9\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
        // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return reg_email.test(this.state.Email);
    }
    _signup = async () => {
        var {
            name,
            pass,
            Email,
        } = this.state
        const data = {
            "name": name,
            "email": Email,
            "password": pass
        }
        if (name == "") this.setState({ error: "* Username can't be empty" });
        else if (!this.validateEmail()) {
            this.setState({ error: "* Invalid Email Id" });
        }
        else if (pass == "")
            this.setState({ error: "* Password can't be empty" });
        else if (pass.length < 6)
            this.setState({ error: "* Password should be 6 or more then 6 char" });
        else {
            // const result = await this.props.saveUser(data);
            // if (result.status === 200) {
            //     this.props.navigation.navigate('OTP')
            // } else {

            //     const err = result.data.data[0];
            //     console.log('eerrrr',err);
                
            //     alert(err.msg);
            // }
            //alert('logged');
            this.props.saveUser(data, () => this.props.navigation.navigate('OTP'));
            //await AsyncStorage.setItem('logged', '1');
            //this.props.navigation.navigate('App');


        }
    }

    setPasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }
    render() {
        return (
            <View style={{
                flex: 1,
                backgroundColor: "transparent",
                justifyContent: "center"
            }}>
                <StatusBar hidden />
                <ImageBackground style={styles.imgBackground}
                    resizeMode='stretch'
                    source={require('../../assets/Background.png')}>

                    <View style={{
                        flex: 1,
                        backgroundColor: "transparent",
                        justifyContent: "center"
                    }}>
                        <ScrollView
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}>
                            <Image style={{ marginTop: verticalScale(79), marginLeft: scale(30), width: scale(198), height: scale(24) }}
                                resizeMode='cover'
                                source={require('../../assets/VACCINELEDGER.png')} />
                            <Text style={{ marginLeft: scale(30), marginTop: verticalScale(8), fontSize: scale(26), color: '#000000', fontFamily: "Roboto-Regular" }}>
                                Welcome
                        </Text>
                            <Text style={{ marginLeft: scale(30), marginTop: verticalScale(9), fontSize: scale(13), color: '#000000', fontFamily: "Roboto-Light" }}>
                                Signup to continue
                        </Text>

                            <View style={[{ marginLeft: scale(30), marginRight: scale(30), height: scale(391), borderRadius: 10, backgroundColor: "#FFFFFF", marginTop: verticalScale(40), justifyContent: "center", alignItems: "center" }, shadowStyle]}>
                                <Text style={{ marginTop: verticalScale(20), fontSize: scale(20), color: '#000000', fontFamily: "Roboto-Bold", fontWeight: "bold" }}>
                                    Signup
                        </Text>


                                {/* <View style={{ flexDirection: "row", marginTop: verticalScale(32), width: "100%" }}>
                                <Image style={{ width: scale(15.29), height: scale(17.4), marginLeft: scale(34) }}
                                    resizeMode='cover'
                                    source={require('../../assets/user.png')} />
                                <Text style={{ marginLeft: scale(10.71), fontSize: scale(15), color: '#707070', fontFamily: "Roboto-Regular" }}>
                                    Username
                        </Text>
                            </View> */}
                                <View style={{ width: "100%", }}>
                                    <FloatingLabelInput
                                        label="Name"
                                        onChangeText={(name) => this.setState({ name })}
                                        value={this.state.name}
                                        image={require('../../assets/user.png')}
                                    />
                                    {/* <TextInput
                                    style={{ height: scale(30), marginLeft: scale(34), marginRight: scale(34), backgroundColor: "transparent", borderBottomWidth: 2, borderBottomColor: "#D6D6D6", marginTop: verticalScale(13) }}
                                    onChangeText={(name) => this.setState({ name })}
                                    value={this.state.name}
                                /> */}
                                </View>

                                <View style={{ width: "100%", marginTop: verticalScale(10) }}>
                                    <FloatingLabelInput
                                        label="Email"
                                        onChangeText={(Email) => this.setState({ Email })}
                                        value={this.state.Email}
                                        image={require('../../assets/mail.png')}
                                        keyboardType={"email-address"}
                                    />
                                </View>

                                <View style={{ width: "100%", marginTop: verticalScale(10) }}>
                                    <FloatingLabelInput
                                        label="Password"
                                        onChangeText={(pass) => this.setState({ pass })}
                                        value={this.state.pass}
                                        secureTextEntry={this.state.hidePassword}
                                        image={require('../../assets/key.png')}
                                        password={true}
                                        passimage={(this.state.hidePassword) ? require('../../assets/HidePassword.png') : require('../../assets/ShowPassword.png')}
                                        onClick={this.setPasswordVisibility}
                                    />
                                </View>

                                <Text style={{ color: "red", marginTop: verticalScale(1), fontSize: scale(12), fontFamily: "Roboto-Bold", alignSelf: "center" }}>{this.state.error}</Text>

                                <TouchableOpacity
                                    onPress={this._signup}
                                    style={{ width: scale(133), height: scale(40), borderRadius: 8, backgroundColor: "red", justifyContent: "center", alignItems: "center" }}>
                                    <LinearGradient
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                        colors={['#0093E9', '#36C2CF']}
                                        style={{ width: scale(133), height: scale(40), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={{ fontSize: scale(20), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>SIGNUP</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <View style={{ justifyContent: "center", alignItems: "center", flexDirection: "row", marginTop: verticalScale(17) }}>
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#707070" }}>
                                        Already have an Account?
                </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Auth')}>
                                        <Text style={{ marginLeft: scale(7), fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#0B65C1", fontWeight: "bold" }}>
                                            Log In
                </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ height: scale(10) }} />
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
        position: 'absolute',
    },
    wrapper: {
        flex: 1,
    },
})
function mapStateToProps(state) {
    return {
        userdata: state.register.userdata
    }
}

export default connect(mapStateToProps, { saveUser })(Signup)