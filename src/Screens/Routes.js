import * as React from 'react';
import { Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation-tabs';

import { scale, moderateScale, verticalScale } from './../components/Scale';

import SplashScreen from "./SplashScreen";
import AuthLoadingScreen from "./AuthLoadingScreen";
import Signin from "./Signin";
import Signup from "./Signup";
import OTP from "./Signup/OTP"
import Shipment from "./Shipment";
import Shipment_Orders from "./Shipment_Orders";
import Shipment_Order_ShowMore from "../Screens/Shipment_Orders/Shipment_Order_ShowMore";
import Inventory from "./Inventory";
import Inventory_Show_More from "./Inventory/Inventory_Show_More";
import Scan from "./Scan";
import Custom_Side_Menu from "../components/Custom_Side_Menu";
import Profile from "../Screens/Profile";
import Add_Inventory from "../Screens/Inventory/Add_Inventory";


// const ShipmentScreen = createMaterialTopTabNavigator({
//   All_Shipment:
//   {
//       screen: All_Shipment,
//       navigationOptions: {
//           headerShown: false,
//       },
//   },
//   Sent_Shipment:
//   {
//       screen: Sent_Shipment,
//       navigationOptions: {
//           headerShown: false,
//       },
//   },
//   Received_Shipment:
//   {
//       screen: Received_Shipment,
//       navigationOptions: {
//           headerShown: false,
//       },
//   },
// },
//   {
//     tabBarPosition: 'top',
//     swipeEnabled: true,
//     animationEnabled: true,
//     tabBarOptions: {
//       activeTintColor: 'transparent',
//       inactiveTintColor: 'transparent',
//       style: {
//         backgroundColor: 'transparent',
//       },
//       labelStyle: {
//         textAlign: 'center',
//         color:"#000"
//       },
//       indicatorStyle: {
//         borderBottomColor: '#87B56A',
//         borderBottomWidth: 2,
//       },
//     },
// })
const ShipmentScreen = createStackNavigator({
  Shipment: {
    screen: Shipment,
    navigationOptions: {
      headerShown: false,
    },
  }
})

const Shipment_OrdersScreen = createSwitchNavigator({
  Shipment_Orders: {
    screen: Shipment_Orders,
    navigationOptions: {
      headerShown: false,
    },
  },
  Shipment_Order_ShowMore: {
    screen: Shipment_Order_ShowMore,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Shipment_Orders',

  })

const InventoryScreen = createSwitchNavigator({
  Inventory: {
    screen: Inventory,
    navigationOptions: {
      headerShown: false,
    },
  },
  Inventory_Show_More: {
    screen: Inventory_Show_More,
    navigationOptions: {
      headerShown: false,
    },
  },
  Add_Inventory: {
    screen: Add_Inventory,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Inventory',

  })

const ScanScreen = createStackNavigator({
  Scan: {
    screen: Scan,
    navigationOptions: {
      headerShown: false,
    },
  }
})

const BottomTabNavigator = createBottomTabNavigator({
  ORDERS: {
    screen: Shipment_OrdersScreen
  },
  SHIPMENT: {
    screen: ShipmentScreen
  },
  INVENTORY: {
    screen: InventoryScreen
  },
  SCAN: {
    screen: ScanScreen
  }
}, {
  initialRouteName: 'SHIPMENT',
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'SHIPMENT') {
        return <Image
          style={{ width: scale(21.7), height: scale(16.06), borderWidth: 0, }}
          source={focused
            ? require('../assets/Shipment1.png') : require('../assets/Shipment.png')}
          resizeMode='contain' />;
      } else if (routeName === 'INVENTORY') {
        return <Image
          style={{ width: scale(18), height: scale(18), borderWidth: 0, }}
          source={focused
            ? require('../assets/Inventory1.png') : require('../assets/Inventory.png')}
          resizeMode='contain' />;
      } else if (routeName === 'ORDERS') {
        return <Image
          style={{ width: scale(16.59), height: scale(18), borderWidth: 0, }}
          source={focused
            ? require('../assets/order1.png') : require('../assets/order.png')}
          resizeMode='contain' />;
      } else if (routeName === 'SCAN') {
          return <Image
            style={{ width: scale(18), height: scale(18), borderWidth: 0, }}
            source={focused
              ? require('../assets/Scan1.png') : require('../assets/Scan.png')}
            resizeMode='contain' />;
      }
    },
  }),
  tabBarOptions: {
    activeTintColor: '#0093E9',
    inactiveTintColor: '#A8A8A8',
    style: {
      backgroundColor: '#FFFFFF',
      height: scale(45)
    }
  },
});

const HamburgerNavigation = createDrawerNavigator(

  {
    BottomTabNavigator: {
      screen: BottomTabNavigator,
      navigationOptions: {
        headerShown: false,
        tabBarVisible: false,
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'BottomTabNavigator',
    contentComponent: Custom_Side_Menu,
    drawerWidth: scale(250),
  },
);


const AuthStack = createStackNavigator({
  Signin:
  {
    screen: Signin,
    navigationOptions: {
      headerShown: false,
    },
  }
})

const Root = createSwitchNavigator({
  SplashScreen: SplashScreen,
  AuthLoading: AuthLoadingScreen,
  App: HamburgerNavigation,
  Auth: AuthStack
},
  {
    initialRouteName: 'SplashScreen',
  })

const SignupScreens = createSwitchNavigator({
  Signup: {
    screen: Signup,
    navigationOptions: {
      headerShown: false,
    },
  },
  OTP: {
    screen: OTP,
    navigationOptions: {
      headerShown: false,
    },
  },
},
  {
    initialRouteName: 'Signup',
  })

const RootStack = createStackNavigator({
  Root: {
    screen: Root,
    navigationOptions: {
      headerShown: false,
    },
  },
  Signup:
  {
    screen: SignupScreens,
    navigationOptions: {
      headerShown: false,
    },
  },
  Drawer: {
    screen: HamburgerNavigation,
    navigationOptions: {
      headerShown: false,
    },
  },
  // Profile: {
  //   screen: Profile,
  //   navigationOptions: {
  //     headerShown: false,
  //   },
  // },
},
  {
    initialRouteName: 'Root',
  })

const AppContainer = createAppContainer(RootStack);

export default class Routes extends React.Component {
  render() {
    return <AppContainer />;
  }
}