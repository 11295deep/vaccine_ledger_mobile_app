import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    StatusBar,
    ImageBackground,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    BackHandler,
    LayoutAnimation,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import Header from "../../components/Header"
import HamburgerIcon from '../../components/HamburgerIcon';

class Inventory extends React.Component {
    constructor(props) {
        super(props);
        this.arrayholder = [];
        this.state = {
            text: '',
            expanded: false,
            data2: [{
                vaccine: "BCG",
                fullname: "Bacille Calmette Guerin Vaccine",
                qty: "20,000",
                color: "#D9E5EF",
            },
            {
                vaccine: "OPV",
                fullname: "Oral Polio Vaccine",
                qty: "20,000",
                color: "#FFBCC4",
            },
            {
                vaccine: "HepB",
                fullname: "Hepatitis B Vaccine",
                qty: "20,000",
                color: "#FFEF83",
            },
            {
                vaccine: "DTwP",
                fullname: "Diphtheria and tetanus toxoids & whole-cell pertussis Vaccine",
                qty: "20,000",
                color: "#FFD0CA",
            },
            {
                vaccine: "IPV",
                fullname: "Inactivated Polio Vaccine",
                qty: "20,000",
                color: "#EBDDED",
            },
            {
                vaccine: "HiB",
                fullname: "Haemophilus influenzae Type B Vaccine",
                qty: "20,000",
                color: "#F1DDC6",
            },
            {
                vaccine: "RV",
                fullname: "Rotavirus Vaccine",
                qty: "20,000",
                color: "#D4EEEC",
            },
            {
                vaccine: "PVC",
                fullname: "Pneumococcal conjugate Vaccine",
                qty: "20,000",
                color: "#FFC18C",
            },
            {
                vaccine: "MMR",
                fullname: "Mumps, Measles & Rubella Vaccine",
                qty: "20,000",
                color: "#C1E3F2",
            },],
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Shipment');
        return true;
    }

    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        console.log('async', token);
        console.log('redux', this.props.logindata);

    }

    SearchFilterFunction = (text) => {
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            text: text,
        });
    }

    changeLayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ expanded: !this.state.expanded });
    }

    render() {
        const {text} = this.state;
        //passing the inserted text in textinput
        let inventorydata = this.props.inventorydata.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.productName ? item.productName.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
            //return item.productName.toLowerCase().indexOf(text.toLowerCase()) !== -1
        });
        console.log('render_inventorydata',this.props.inventorydata);
        this.arrayholder = this.props.inventorydata
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Inventory'} />

                <View style={{ width: scale(328), height: scale(32), backgroundColor: "#FFFFFF", borderRadius: 8, flexDirection: "row", marginTop: verticalScale(-71), justifyContent: "center", alignItems: "center" }}>
                    <TextInput style={{ width: "80%", }}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        value={this.state.text}
                    />
                    <Image style={{ width: scale(16.85), height: scale(16.85), marginLeft: scale(15), }}
                        resizeMode='center'
                        source={require('../../assets/Search.png')} />
                </View>

                <View style={{ width: scale(328), height: scale(69), backgroundColor: "#FFFFFF", borderRadius: 8, flexDirection: "row", marginTop: verticalScale(18), }}>

                    <Image style={{ width: scale(22.15), height: scale(22.15), marginLeft: scale(20), marginTop: verticalScale(18) }}
                        resizeMode='center'
                        source={require('../../assets/Inventory1.png')} />

                    <View style={{ marginLeft: scale(16.85), marginTop: verticalScale(18), width: "50%" }}>
                        <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Total Inventory</Text>
                        <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", marginTop: scale(7) }}>Today</Text>
                    </View>

                    <View style={{ marginTop: verticalScale(18), }}>
                        <Text style={{ fontSize: scale(19), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>10,35,000</Text>
                    </View>
                </View>

                <View style={{ width: scale(328), height: scale(40), backgroundColor: "transparent", marginTop: verticalScale(18), }}>
                    <TouchableOpacity style={{ width: scale(149), height: scale(40), borderRadius: 10, backgroundColor: "#FFAB1D", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                    onPress={()=>this.props.navigation.navigate('Add_Inventory')}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../assets/add.png')} />
                        <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>Add Inventory</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ height: 5 }} />

                <ScrollView>
                    <View style={{ alignItems: "center" }}>

                        <View style={{ width: scale(328), height: scale(25), backgroundColor: "transparent", marginTop: verticalScale(20), flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                            <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Product List</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Inventory_Show_More')}>
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", }}>SHOW MORE</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ width: Dimensions.get('window').width, }}>
                            <FlatList
                                data={this.state.data2}
                                extraData={this.state}
                                enableEmptySections={true}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                keyExtractor={(item, index) => index.toString()}
                                horizontal={true}
                                renderItem={({ item, index }) => (
                                    <View style={{ width: scale(99), height: scale(110), justifyContent: "center", alignItems: "center", backgroundColor: "#FFFFFF", marginLeft: scale(12), marginTop: verticalScale(15), borderRadius: 8 }}>
                                        <View style={{ width: scale(40), height: scale(40), borderRadius: 400, backgroundColor: item.color, justifyContent: "center", alignItems: "center" }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{item.vaccine}</Text>
                                        </View>
                                        <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#000000", marginTop: verticalScale(5), marginBottom: verticalScale(20), width: scale(74), textAlign: "center" }} numberOfLines={2}>{item.fullname}</Text>

                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#000000", bottom: verticalScale(7), position: 'absolute', }}>Qty: {item.qty}</Text>
                                    </View>
                                )}

                            />
                        </View>

                        <View style={{ width: scale(328), height: scale(25), backgroundColor: "transparent", marginTop: verticalScale(20), flexDirection: "row", justifyContent: "space-between" }}>
                            <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Recent Inventory</Text>
                            <TouchableOpacity style={{ width: scale(81), height: scale(25), borderRadius: 10, backgroundColor: "#36C2CF", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                            >
                                <Image style={{ width: scale(9.35), height: scale(11.68), }}
                                    resizeMode='center'
                                    source={require('../../assets/Filter.png')} />
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "900" }}>Filter</Text>
                                <Image style={{ width: scale(10.01), height: scale(5.72), }}
                                    resizeMode='cover'
                                    source={require('../../assets/downarrow.png')} />
                            </TouchableOpacity>
                        </View>

                        <FlatList
                            data={inventorydata}
                            extraData={this.state}
                            enableEmptySections={true}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <View>
                                    {index <= 1 ?
                                        <View style={{ marginTop: verticalScale(15), width: scale(328), height: scale(96), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                                            <View style={{ flexDirection: "row", alignItems: "center", height: scale(25), marginLeft: scale(10), marginTop: verticalScale(10), }}>
                                                <View style={{ width: scale(25), height: scale(25), borderRadius: 400, backgroundColor: item.productName === "BCG" ? "#D9E5EF" : item.productName === "bOPV"? "#FFBCC4":item.productName === "OPV"? "#FFBCC4":item.productName === "HepB"? "#FFEF83" : item.productName === "DTwP"? "#FFD0CA":item.productName === "IPV"? "#EBDDED":item.productName === "HiB"? "#F1DDC6":item.productName === "RV"? "#D4EEEC":item.productName === "PVC"? "#FFC18C":item.productName === "MMR"? "#C1E3F2" : "#FF0000", justifyContent: "center", alignItems: "center" }}>
                                                    <Text style={{ fontSize: scale(8), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{item.productName}</Text>
                                                </View>
                                                <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(8), width: "70%" }}>{item.productName}</Text>
                                                <Text style={{ fontSize: scale(12), color: "#000000", fontFamily: "Roboto-Bold", fontWeight: "bold" }}>{item.quantity}</Text>
                                            </View>
                                            <View style={{ flexDirection: "row", marginLeft: scale(43), height: scale(15), width: "100%"}}>
                                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#7070707", }}>Manufacturer</Text>
                                                <Text style={{ marginLeft:scale(11),fontSize: scale(12), color: "#000000", fontFamily: "Roboto-Regular" }} numberOfLines={1}>{item.manufacturerName}</Text>
                                            </View>
                                            <View style={{ flexDirection: "row", marginLeft: scale(43), height: scale(15), width: "80%", marginTop: verticalScale(10), }}>
                                                <View style={{ flexDirection: "row", width: "35%", justifyContent: "space-between", }}>
                                                    <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#7070707", }}>Mfg Date</Text>
                                                    <Text style={{ fontSize: scale(10), color: "#000000", fontFamily: "Roboto-Regular" }}>{item.manufacturingDate}</Text>
                                                </View>
                                                <View style={{ flexDirection: "row", marginLeft: scale(50), width: "35%", justifyContent: "space-between" }}>
                                                    <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#7070707", }}>Exp Date</Text>
                                                    <Text style={{ fontSize: scale(10), color: "#000000", fontFamily: "Roboto-Regular" }}>{item.expiryDate}</Text>
                                                </View>
                                            </View>
                                        </View> :
                                        <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden', }}>
                                            {index > 1 ?
                                                <View style={{ marginTop: verticalScale(15), width: scale(328), height: scale(96), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                                                <View style={{ flexDirection: "row", alignItems: "center", height: scale(25), marginLeft: scale(10), marginTop: verticalScale(10), }}>
                                                    
                                                <View style={{ width: scale(25), height: scale(25), borderRadius: 400, backgroundColor: item.productName === "BCG" ? "#D9E5EF" : item.productName === "bOPV"? "#FFBCC4":item.productName === "OPV"? "#FFBCC4":item.productName === "HepB"? "#FFEF83" : item.productName === "DTwP"? "#FFD0CA":item.productName === "IPV"? "#EBDDED":item.productName === "HiB"? "#F1DDC6":item.productName === "RV"? "#D4EEEC":item.productName === "PVC"? "#FFC18C":item.productName === "MMR"? "#C1E3F2" : "#FF0000", justifyContent: "center", alignItems: "center" }}>
                                                        <Text style={{ fontSize: scale(8), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{item.productName}</Text>
                                                    </View>
                                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(8), width: "70%" }}>{item.productName}</Text>
                                                    <Text style={{ fontSize: scale(12), color: "#000000", fontFamily: "Roboto-Bold", fontWeight: "bold" }}>{item.quantity}</Text>
                                                </View>
                                                <View style={{ flexDirection: "row", marginLeft: scale(43), height: scale(15), width: "100%",}}>
                                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#7070707", }}>Manufacturer</Text>
                                                    <Text style={{ marginLeft:scale(11),fontSize: scale(12), color: "#000000", fontFamily: "Roboto-Regular" }} numberOfLines={1}>{item.manufacturerName}</Text>
                                                </View>
                                                <View style={{ flexDirection: "row", marginLeft: scale(43), height: scale(15), width: "80%", marginTop: verticalScale(10), }}>
                                                    <View style={{ flexDirection: "row", width: "35%", justifyContent: "space-between", }}>
                                                        <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#7070707", }}>Mfg Date</Text>
                                                        <Text style={{ fontSize: scale(10), color: "#000000", fontFamily: "Roboto-Regular" }}>{item.manufacturingDate}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row", marginLeft: scale(50), width: "35%", justifyContent: "space-between" }}>
                                                        <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#7070707", }}>Exp Date</Text>
                                                        <Text style={{ fontSize: scale(10), color: "#000000", fontFamily: "Roboto-Regular" }}>{item.expiryDate}</Text>
                                                    </View>
                                                </View>
                                            </View> : null}
                                        </View>}
                                </View>
                            )}

                        />


                        <View style={{ marginTop: verticalScale(15) }}>
                            <View style={{ width: scale(328), borderWidth: 1, borderColor: "#D6D6D6" }} />
                            <TouchableOpacity style={{ marginTop: verticalScale(10), justifyContent: "center", alignItems: "center" }}
                                onPress={this.changeLayout}>
                                {!this.state.expanded ?
                                    <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", }}>SHOW MORE</Text> :
                                    <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", }}>SHOW LESS</Text>}
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: scale(10) }} />
                    </View>
                    
                </ScrollView>

            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        inventorydata:state.inventory.inventorydata,
    }
}

export default connect(mapStateToProps, {})(Inventory)