import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    StatusBar,
    // Picker,
    ImageBackground,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    BackHandler,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { add_inventory } from '../../../Action';
import Picker from 'react-native-picker';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Header from "../../../components/Header"
import HamburgerIcon from '../../../components/HamburgerIcon';
import Inventory from '..';

class Add_Inventory extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            data: [{
                productName: "",
                manufacturerName: "",
                quantity: "",
                manufacturingDate: "",
                expiryDate: "",
                storageCondition: "",
                batchNumber: "",
                serialNumber: ""
            }]
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Inventory');
        return true;
    }

    _createDateData() {
        let date = [];
        for (let i = 2016; i < 2050; i++) {
            let month = [];
            for (let j = 1; j < 13; j++) {
                let _month = {};
                _month[j] = j;
                month.push(j);
            }
            let _date = {};
            _date[i] = month;
            date.push(_date);
        }
        return date;
    }

    _createCelsiusData(){
        let Celsius = [];
        for (let i = -30; i < 31; i++) {
            let left = [];
            for (let j = -30; j < 31; j++) {
                let _left = {};
                _left[j] = j;
                left.push(j+'°C');
            }
            let _right = {};
            _right[i+'°C'] = left;
            Celsius.push(_right);
        }
        return Celsius;
    }

    _showCelsiusPicker(index) {
        Picker.init({
            pickerData: this._createCelsiusData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Selet Storage Conditions",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[0] + "  -  " + pickedValue[1]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].storageCondition = date
                this.setState({data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[0] + "  -  " + pickedValue[1]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].storageCondition = date
                this.setState({data: productClone })
            }
        });
        Picker.show();
    }

    _showMfgDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Selet Manufacturing Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturingDate = date
                this.setState({data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturingDate = date
                this.setState({data: productClone })
            }
        });
        Picker.show();
    }

    _showExpDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Selet Expiry Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].expiryDate = date
                this.setState({data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].expiryDate = date
                this.setState({data: productClone })
            }
        });
        Picker.show();
    }

    _showProductName(index) {
        Picker.init({
            pickerData: ["BCG", "OPV", "HepB", "IPV"],
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Product",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].productName = value
                this.setState({data: productClone })
                
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].productName = value
                this.setState({data: productClone })
            }
        });
        Picker.show();
    }

    _showManufacturer(index) {
        Picker.init({
            pickerData: ["M1", "M2", "M3", "M4"],
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Select Manufacturer",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturerName = value
                this.setState({data: productClone })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);
            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var value = pickedValue[0]
                const productClone = JSON.parse(JSON.stringify(this.state.data));
                productClone[index].manufacturerName = value
                this.setState({data: productClone })

            }
        });
        Picker.show();
    }

    _add_another_inventory() {
        this.setState({
            data: [...this.state.data, {
                productName: '',
                manufacturerName: '',
                quantity: '',
                manufacturingDate: "",
                expiryDate: "",
                storageCondition: "",
                batchNumber: "",
                serialNumber: ""
            }]
        })
    }

    updateValue(index, item) {
        this.setState({
            data: [
                ...this.state.data.slice(0, index),
                Object.assign({}, this.state.data[index], item),
                ...this.state.data.slice(index + 1)
            ]
        });
        console.log('data', this.state.data);
    }

    _SaveInventory = async () =>{
        console.log('save',this.state.data);
        console.log('bbbbbb',this.props.inventorydata);
        //const result = 
        // let response = Promise.all(this.state.data.map(elem=>{
        //     axios.post(url,{data:elem},)
        // }))
        await this.props.add_inventory(this.state.data,() => this.props.navigation.navigate('Inventory'))
        // if (result.status === 200) { this.setState({ edit: false })}
        console.log('sssss',this.props.inventorydata);
        
        
    }

    render() {
        console.log('render',this.props.inventorydata);
        var Inventory_Details = [];
        for (let i = 0; i < this.state.data.length; i++) {
            Inventory_Details.push(
                <View style={{ marginTop: verticalScale(20), width: scale(328), height: scale(650), backgroundColor: "#FFFFFF", borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                    <View style={{ marginLeft: scale(16), marginRight: scale(16) }}>

                        <View style={{ flexDirection: "row", height: scale(35), marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Product Name</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={()=>this._showProductName(i)}>
                                <Text style={{ width: "85%",color: this.state.data[i].productName === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].productName === "" ? "Select Product" : this.state.data[i].productName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Manufacturer</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={()=>this._showManufacturer(i)}
                            >
                                <Text style={{ width: "85%",color: this.state.data[i].manufacturerName === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].manufacturerName === "" ? "Select Manufacturer" : this.state.data[i].manufacturerName}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>quantity</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter quantity"
                                    keyboardType="number-pad"
                                    value={this.state.data[i].quantity}
                                    onChangeText={(quantity) => this.updateValue(i, { quantity })} />
                            </View>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Mfg Date</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={()=>this._showMfgDatePicker(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].manufacturingDate === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].manufacturingDate === "" ? "MM/YYYY" : this.state.data[i].manufacturingDate}</Text>
                                <Image
                                    style={{ width: scale(14.04), height: scale(14.04), }}
                                    source={require("../../../assets/calendar.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>


                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Exp Date</Text>
                            </View>

                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={()=>this._showExpDatePicker(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].expiryDate === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].expiryDate === "" ? "MM/YYYY" : this.state.data[i].expiryDate}</Text>
                                <Image
                                    style={{ width: scale(14.04), height: scale(14.04), }}
                                    source={require("../../../assets/calendar.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>


                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Storage Conditions</Text>
                            </View>

                            {/* <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Storage Conditions"
                                    value={this.state.data[i].storageCondition}
                                    onChangeText={(storageCondition) => this.updateValue(i, { storageCondition })} />
                            </View> */}
                            <TouchableOpacity style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8", alignItems: "center", flexDirection: "row" }}
                                onPress={()=>this._showCelsiusPicker(i)}>
                                <Text style={{ width: "85%", color: this.state.data[i].storageCondition === "" ? "#A8A8A8" : "#000" }}>{this.state.data[i].storageCondition === "" ? "Enter Storage Conditions" : this.state.data[i].storageCondition}</Text>
                                <Image
                                    style={{ width: scale(10.2), height: scale(14.09), }}
                                    source={require("../../../assets/updown.png")}
                                    resizeMode='contain'
                                />
                            </TouchableOpacity>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Batch Number</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Batch Number"
                                    value={this.state.data[i].batchNumber}
                                    onChangeText={(batchNumber) => this.updateValue(i, { batchNumber })} />
                            </View>

                        </View>

                        <View style={{ flexDirection: "row", height: scale(35), justifyContent: "center", alignItems: "center", marginTop: verticalScale(30) }}>
                            <View style={{ width: "50%", height: scale(35), justifyContent: "center", }}>
                                <Text style={{ color: "#707070", }}>Serial Numbers</Text>
                            </View>

                            <View style={{ width: "50%", height: scale(35), borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}>
                                <TextInput style={{ borderBottomWidth: 1, borderBottomColor: "#E8E8E8" }}
                                    placeholder="Enter Serial Numbers"
                                    value={this.state.data[i].serialNumber}
                                    onChangeText={(serialNumber) => this.updateValue(i, { serialNumber })} />
                            </View>

                        </View>

                        <TouchableOpacity
                            style={{ marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", alignSelf: "flex-end" }}>
                            <Image style={{ width: scale(15), height: scale(15) }}
                                resizeMode='center'
                                source={require('../../../assets/Scan1.png')} />
                            <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>SCAN</Text>
                        </TouchableOpacity>

                    </View>
                </View>)
        }
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Add Inventory'} />
                <ScrollView nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-71) }}>

                    {Inventory_Details}

                    <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(58), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}
                        onPress={() => this._add_another_inventory()}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../../assets/plus.png')} />
                        <Text style={{ marginLeft: scale(10), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>Add Another Product</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginTop: verticalScale(21), width: scale(328), height: scale(40), backgroundColor: "#FFAB1D", borderRadius: 10, flexDirection: "row", alignItems: "center", justifyContent: "center" }}
                        onPress={() => this._SaveInventory()}>
                        <Image style={{ width: scale(13.73), height: scale(13.73), }}
                            resizeMode='center'
                            source={require('../../../assets/add.png')} />
                        <Text style={{ marginLeft: scale(13), fontSize: scale(16), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>Add Inventory</Text>
                    </TouchableOpacity>

                    <View style={{ height: scale(25) }} />
                </ScrollView>
            </View >
        )
    }
}
function mapStateToProps(state) {
    return {
        user: state.userinfo.user,
        inventorydata:state.inventory.inventorydata,
    }
}

export default connect(mapStateToProps, { add_inventory })(Add_Inventory)