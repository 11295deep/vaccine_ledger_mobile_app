import React from 'react';
import {
    StyleSheet,
    Dimensions,
    StatusBar,
    BackHandler,
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import LinearGradient from 'react-native-linear-gradient';

import { scale, moderateScale, verticalScale } from '../../components/Scale';
import HamburgerIcon from '../../components/HamburgerIcon';
import Header from "../../components/Header"
import All_Shipment from "./All_Shipment";
import Sent_Shipment from "./Sent_Shipment";
import Received_Shipment from "./Received_Shipment";

export default class Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            routes: [
                { key: 'All', title: ' All ' },
                { key: 'Sent', title: ' Sent ' },
                { key: 'Received', title: ' Received ' },
            ],
        };
    }
    render() {
        return (
            <View style={{
                flex: 1,
                //backgroundColor: "#000",
                justifyContent: "center",
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Shipment'} />
                {/* <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={['#0093E9', '#36C2CF']}
                    style={{ width: Dimensions.get('window').width, height: scale(180), borderBottomLeftRadius: 24, borderBottomRightRadius: 24 }}>
                    <View style={{marginTop:verticalScale(50),marginLeft:scale(16),flexDirection:"row",alignItems:"center",}}>
                        <HamburgerIcon />
                        <Text style={{marginLeft:scale(36),fontSize:scale(24),fontFamily:"Roboto-Bold",color:"#FFFFFF",width:"75%"}}>Shipment</Text>

                        <Image
                            style={{ width: scale(13.5), height: scale(15),alignSelf:"center"}}
                            source={require("../../assets/Bell.png")}
                            resizeMode='contain'
                        />
                    </View>
                    </LinearGradient> */}
            {/* <View style={{height:Dimensions.get('window').height}}>
                    <TabView
                        navigationState={this.state}
                        renderScene={SceneMap({
                            All: () => (
                                <All_Shipment navigation={this.props.navigation} />
                            ),
                            Sent: Sent_Shipment,
                            Received: Received_Shipment,
                        })}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ width: Dimensions.get('window').width }}
                        renderTabBar={(props) => (
                            <TabBar
                                {...props}
                                indicatorStyle={styles.indicator}
                                tabStyle={{alignSelf:"flex-start",width: 'auto'}}
                                //indicatorContainerStyle={{backgroundColor:"red"}}
                                activeColor="#FFFFFF"
                                inactiveColor="#C8C8C8"
                                style={styles.tabbar}
                                labelStyle={{fontSize: scale(14),fontWeight:"bold"}}
                            />
                        )}
                    />
                    </View> */}
                {/* </LinearGradient> */}
                <View style={{marginTop: scale(-90),height:Dimensions.get('window').height-scale(100) }}>
                    <TabView
                        navigationState={this.state}
                        renderScene={SceneMap({
                            All: () => (
                                <All_Shipment navigation={this.props.navigation} />
                            ),
                            Sent: Sent_Shipment,
                            Received: Received_Shipment,
                        })}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ width: Dimensions.get('window').width }}
                        renderTabBar={(props) => (
                            <TabBar
                                {...props}
                                indicatorStyle={styles.indicator}
                                tabStyle={{alignSelf:"flex-start",width: 'auto'}}
                                //indicatorContainerStyle={{backgroundColor:"red"}}
                                activeColor="#FFFFFF"
                                inactiveColor="#C8C8C8"
                                style={styles.tabbar}
                                labelStyle={{fontSize: scale(14),fontWeight:"bold"}}
                            />
                        )}
                    />
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f4f7f9',
    },
    tabbar: {
        backgroundColor: 'transparent',
        width: scale(230),
        height: scale(40),
        //backgroundColor: "#000",

    },
    indicator: {
        backgroundColor: '#FFFFFF',
        width:(10),
        borderRadius:3,
        marginLeft:scale(17)
        
    },
});
