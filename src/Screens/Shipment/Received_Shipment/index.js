import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    LayoutAnimation, UIManager,
    FlatList,
    Platform,
    Switch,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import Shipment_Card from "../../../components/Shipment_Card";

class Received_Shipment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:
                [
                    {
                        image: require('../../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        quantity: '+4,00,000',
                        status: 'Received',
                        client: "UNICEF",
                        transaction_id: "wsdhcuwgfhwfciu928374jnd",
                        vaccines: [{
                            vaccine: "Hib",
                            Manufacturer: "GSK",
                            Qty: "50,000",
                            fullname: "Haemophilus influenzae Type B Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#F1DDC6"
                        },
                        {
                            vaccine: "OPV",
                            Manufacturer: "GSK",
                            Qty: "3,50,000",
                            fullname: "Oral Polio Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#FFBCC4"
                        },]
                    },

                    {
                        image: require('../../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        quantity: '+3,00,000',
                        status: 'Received',
                        client: "UNICEF",
                        transaction_id: "wsdhcuwgfhwfciu928374jnd",
                        vaccines: [{
                            vaccine: "Hib",
                            Manufacturer: "GSK",
                            Qty: "50,000",
                            fullname: "Haemophilus influenzae Type B Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#F1DDC6"
                        },
                        {
                            vaccine: "OPV",
                            Manufacturer: "GSK",
                            Qty: "3,50,000",
                            fullname: "Oral Polio Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#FFBCC4"
                        },]
                    },

                    {
                        image: require('../../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        quantity: '+2,00,000',
                        status: 'Received',
                        client: "UNICEF",
                        transaction_id: "wsdhcuwgfhwfciu928374jnd",
                        vaccines: [{
                            vaccine: "Hib",
                            Manufacturer: "GSK",
                            Qty: "50,000",
                            fullname: "Haemophilus influenzae Type B Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#F1DDC6"
                        },
                        {
                            vaccine: "OPV",
                            Manufacturer: "GSK",
                            Qty: "3,50,000",
                            fullname: "Oral Polio Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#FFBCC4"
                        },]
                    },
                    {
                        image: require('../../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        quantity: '+4,00,000',
                        status: 'Received',
                        client: "UNICEF",
                        transaction_id: "wsdhcuwgfhwfciu928374jnd",
                        vaccines: [{
                            vaccine: "Hib",
                            Manufacturer: "GSK",
                            Qty: "50,000",
                            fullname: "Haemophilus influenzae Type B Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#F1DDC6"
                        },
                        {
                            vaccine: "OPV",
                            Manufacturer: "GSK",
                            Qty: "3,50,000",
                            fullname: "Oral Polio Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#FFBCC4"
                        },]
                    },

                    {
                        image: require('../../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        quantity: '+3,00,000',
                        status: 'Received',
                        client: "UNICEF",
                        transaction_id: "wsdhcuwgfhwfciu928374jnd",
                        vaccines: [{
                            vaccine: "Hib",
                            Manufacturer: "GSK",
                            Qty: "50,000",
                            fullname: "Haemophilus influenzae Type B Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#F1DDC6"
                        },
                        {
                            vaccine: "OPV",
                            Manufacturer: "GSK",
                            Qty: "3,50,000",
                            fullname: "Oral Polio Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#FFBCC4"
                        },]
                    },

                    {
                        image: require('../../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        quantity: '+2,00,000',
                        status: 'Received',
                        client: "UNICEF",
                        transaction_id: "wsdhcuwgfhwfciu928374jnd",
                        vaccines: [{
                            vaccine: "Hib",
                            Manufacturer: "GSK",
                            Qty: "50,000",
                            fullname: "Haemophilus influenzae Type B Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#F1DDC6"
                        },
                        {
                            vaccine: "OPV",
                            Manufacturer: "GSK",
                            Qty: "3,50,000",
                            fullname: "Oral Polio Vaccine",
                            Mfg_Date: "02/2020",
                            Exp_Date: "02/2022",
                            Batch_No: "GSK",
                            Serial_No: "23798771879 -23798821879",
                            color: "#FFBCC4"
                        },]
                    },
                ],
        }
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    updateLayout = index => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.props.shipment];
        array.map((value, placeindex) =>
            placeindex === index
                ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
                : (array[placeindex]['isExpanded'] = false)
        );
        this.setState(() => {
            return {
                listDataSource: array,
            };
        });
    };
    render() {
        return (
            <View style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                //backgroundColor:"#000",

            }}>
                <View style={{ height: 5 }} />
                <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>

                    {this.props.shipment.length === 0 ? <View>
                    <Image style={{ marginTop: verticalScale(100),width: scale(100), height: scale(100),alignSelf:"center" }}
                        resizeMode='cover'
                        source={require('../../../assets/shipping.png')} />
                        <Text style={{fontSize:scale(20)}}>Received Shipments are Empty</Text>
                        </View> :
                        this.props.shipment.map((item, key) => (
                            <>
                                {
                                    item.status === "Received" ?
                                        <Shipment_Card button={item} key={key} onClickFunction={this.updateLayout.bind(this, key)} />
                                        : null
                                }
                            </>
                        ))
                    }
                    <View style={{ height: scale(20) }} />
                </ScrollView>

            </View>
        )
    }
}

const customiseShipmentData = (shipment) => {

    let data = [];
    for (let i in shipment) {
        const Object = {};
        const total = 0;
        // shipment[i].products.forEach((qu, index) => {
        //     total = parseInt(qu.quantity) + total;
        //   });
        if (shipment[i].status === "Received") {
            Object = shipment[i]
            Object['isExpanded'] = false
            data.push(Object);
        }

        // Object['total'] = total
    }

    console.log('data', data);

    // for(let key in shipment){
    //     const Object = {};
    //     shipment[key].forEach(element => {
    //         Object['isExpanded'] = false;
    //     });

    //     Object = shipment
    //     data.push(Object);
    // }

    return data;

}
function mapStateToProps(state) {
    return {
        otpdata: state.otp.otpdata,
        userdata: state.register.userdata,
        logindata: state.login.logindata,
        user: state.userinfo.user,
        loder: state.loder,
        inventorydata: state.inventory.inventorydata,
        shipment: customiseShipmentData(state.shipment.shipmentdata),
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, fetchPublisherShipments })(Received_Shipment)


