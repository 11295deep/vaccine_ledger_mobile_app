import React from 'react';
import {
    Button,
    View,
    Text,
    StatusBar,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, create_shipmemnt } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Header from "../../../components/Header"
import { CameraKitCameraScreen, } from 'react-native-camera-kit';

import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import HamburgerIcon from '../../../components/HamburgerIcon';

class Shipment_Order_ShowMore extends React.Component {
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            //variable to hold the qr value
            id: this.props.navigation.getParam('id', 'NO-User'),
            shipmentId: this.props.navigation.getParam('data', 'NO-User'),
            qrvalue: '',
            opneScanner: false,
            mfg: '',
            exp: '',
            batch: '',
            serial: '',
            hide: false
        }
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Shipment_Orders');
        return true;
    }

    componentDidMount () {
        const { shipmentId} = this.state;
        let clone = this.props.shipment;
        //let index;
        for(var i = 0; i < clone.length;i++){
            console.log(i);
            
        }
        let index = clone.findIndex((id) => id.shipmentId === shipmentId );
        console.log('index',index,shipmentId);
        this.setState({
            id:index
        })
    }

    send_Shipment = async () => {
        // In Transit
        const data = this.props.shipment[this.state.id];
        const vaccine = this.props.shipment[this.state.id].products[`0`]
        if (this.props.shipment[this.state.id].status === "Shipped") {

            
            const products = [{...vaccine,batchNumber:this.state.batch,expiryDate:this.state.exp,manufacturingDate:this.state.mfg,serialNumber:this.state.serial}]

            //const finaldata = { ...data, status: "In Transit" }
             const finaldata = { ...data,status: "In Transit",products }
            console.log('ss', { ...data,status: "In Transit",products });
            
            const result = await this.props.create_shipmemnt(finaldata, this.props.shipment[this.state.id].receiver);
            if (result.status === 200) {
                alert("In Transit Successfully")
                this.props.navigation.navigate('Shipment_Orders')
            }
            console.log('rrrr', result);
        } 
        else{
            alert('Something is Wrong');
        }
        // else {
        //     console.log('ss', { ...data, status: "Received" });
        //     const finaldata = { ...data, status: "Received" }

        //     const result = await this.props.create_shipmemnt(finaldata, this.props.shipment[`0`].receiver);
        //     if (result.status === 200) {
        //         alert("Send Shipment Successfully")
        //         this.props.navigation.navigate('SHIPMENT')
        //     }
        //     console.log('rrrr', result);
        // }

    }

    onBarcodeScan(qrvalue) {
        //called after te successful scanning of QRCode/Barcode
        console.log('qr', qrvalue);
        console.log('json', JSON.parse(qrvalue));
        const value = JSON.parse(qrvalue);
        console.log('ggg', value);
        if (this.props.shipment[this.state.id].shipmentId === value.shippingId) {
            alert("Shipment Id Match")
            this.setState({ qrvalue: value.shippingId });
            this.setState({ opneScanner: false });
            this.setState({ hide: true });
            this.setState({
                mfg: value.mfg,
                exp: value.exp,
                batch: value.batchno,
                serial: value.serialno,
            })
        }
        else {
            alert("Shipment Id Doesn't Match")
            this.setState({ opneScanner: false });
            this.setState({ hide: false });

        }
    }
    onOpneScanner() {
        var that = this;
        //To Start Scanning
        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'CameraExample App Camera Permission',
                        'message': 'CameraExample App needs access to your camera '
                    }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //If CAMERA Permission is granted
                        that.setState({ qrvalue: '' });
                        that.setState({ opneScanner: true });
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            //Calling the camera permission function
            requestCameraPermission();
        } else {
            that.setState({ qrvalue: '' });
            that.setState({ opneScanner: true });
        }
    }
    render() {
        //If qrvalue is set then return this view

         const {shipmentId} = this.state;
         let clone = this.props.shipment;
        let index = clone.findIndex((id) => id.shipmentId === shipmentId );
         console.log('renderindex',index,shipmentId);
         const id = index ;
        
        if (!this.state.opneScanner) {
            return (
                <View style={{
                    flex: 1,
                    alignItems: "center"
                }}>
                    <StatusBar backgroundColor="#0093E9" />
                    <Header name={'Shipment Orders'} />

                    <View style={{ marginTop: verticalScale(-71), width: scale(328), height: scale(117), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                        <View style={{ flexDirection: "row", }}>

                            <View style={{ width: scale(30), marginTop: verticalScale(10), marginLeft: scale(10), borderRadius: 10, height: scale(30) }}>
                                <Image style={{ width: scale(30), height: scale(30) }}
                                    resizeMode='center'
                                    source={require('../../../assets/user.png')} />
                            </View>

                            <View style={{ width: "50%", marginTop: verticalScale(10), marginLeft: scale(10), flexDirection: "column" }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>{this.props.shipment[id].deliveryTo}</Text>

                                <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Shipment ID</Text>
                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000" }}>{this.props.shipment[id].shipmentId}</Text>
                                </View>

                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", }}>
                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Quantity</Text>
                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{this.props.shipment[id].products[`0`].quantity}</Text>
                                </View>

                                <View style={{ marginTop: verticalScale(10), flexDirection: "row", }}>
                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Client</Text>
                                    <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{this.props.shipment[id].client}</Text>
                                </View>

                            </View>

                            <View style={{ width: "28%", marginTop: verticalScale(10), marginLeft: scale(10), marginRight: scale(10), flexDirection: "column" }}>
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", alignSelf: "flex-end" }}>{this.props.shipment[id].estimateDeliveryDate}</Text>

                            </View>

                        </View>
                    </View>
                    {/* </ScrollView> */}
                    {/* <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(7) }}>


                    {
                        this.state.data.map((item, index) => (
                            <View style={{ marginTop: verticalScale(15), width: scale(328), height: scale(96), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                                <View style={{ flexDirection: "row", }}>

                                    <View style={{ marginLeft: scale(15), marginTop: verticalScale(17), width: scale(13), height: scale(13), borderRadius: 10, backgroundColor: item.color}} />

                                    <View style={{ marginLeft: scale(15), marginTop: verticalScale(17), width: "60%", }}>
                                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", }}>{item.vaccine}</Text>
                                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(10) }}>-{item.fullname}</Text>
                                        </View>

                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Manufacturer</Text>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item.Manufacturer}</Text>
                                        </View>

                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Qty</Text>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{item.Qty}</Text>
                                        </View>
                                    </View>

                                    <TouchableOpacity style={{ marginLeft: scale(-20), marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                                        <Image style={{ width: scale(15), height: scale(15) }}
                                            resizeMode='center'
                                            source={require('../../../assets/Scan1.png')} />
                                        <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>SCAN</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ))
                    }
                    <View style={{ height: scale(20) }} />

                </ScrollView> */}

                    <View style={{ marginTop: verticalScale(15), width: scale(328), height: this.state.qrvalue === "" ? scale(96) : scale(210), backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                        <View style={{ flexDirection: "row", }}>

                            <View style={{ marginLeft: scale(10), marginTop: verticalScale(10), width: scale(25), height: scale(25), borderRadius: 400, backgroundColor: "#C1E3F2", justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(8), fontFamily: "Roboto-Bold", color: "#707070", fontWeight: "bold" }}>{this.props.shipment[id].products[`0`].productName}</Text>
                            </View>

                            <View style={{ marginLeft: scale(8), marginTop: verticalScale(15), width: "60%", }}>
                                <View style={{ flexDirection: "row", alignItems: "center" }}>
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", }}> {this.props.shipment[id].products[`0`].productName}</Text>
                                    <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(10) }} numberOfLines={1}></Text>
                                </View>

                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                    <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Manufacturer</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }} numberOfLines={1}>{this.props.shipment[id].products[`0`].manufacturerName}</Text>
                                </View>

                                <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                    <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Qty</Text>
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{this.props.shipment[id].products[`0`].quantity}</Text>
                                </View>

                                {this.state.qrvalue === "" ? null :
                                    <View>
                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Mfg Date</Text>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{this.state.mfg}</Text>
                                        </View>
                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Exp Date</Text>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{this.state.exp}</Text>
                                        </View>
                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Batch No</Text>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }}>{this.state.batch}</Text>
                                        </View>
                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(7) }}>
                                            <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070", width: "50%" }}>Serial No</Text>
                                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", width: "50%" }} numberOfLines={1}>{this.state.serial}</Text>
                                        </View>
                                    </View>}
                            </View>

                            {!this.state.hide ?
                                <TouchableOpacity onPress={() => this.onOpneScanner()}
                                    style={{ marginLeft: scale(-10), marginTop: verticalScale(40), width: scale(90), height: scale(35), borderRadius: 8, borderWidth: 1, borderColor: "#0093E9", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                                    <Image style={{ width: scale(15), height: scale(15) }}
                                        resizeMode='center'
                                        source={require('../../../assets/Scan1.png')} />
                                    <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>SCAN</Text>
                                </TouchableOpacity> : null}
                        </View>
                    </View>

                    {this.state.qrvalue === this.props.shipment[id].shipmentId ?
                        <TouchableOpacity onPress={() => { this.send_Shipment() }}
                            style={{ width: scale(328), height: scale(40), borderRadius: 8, marginTop: verticalScale(90), position: 'absolute', bottom: 0, }}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#0093E9', '#36C2CF']}
                                style={{ width: scale(328), height: scale(40), borderRadius: 8, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(15), fontFamily: "Roboto-Bold", color: "#FFFFFF", fontWeight: "bold" }}>Send Shipment</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        : null}
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <CameraKitCameraScreen
                    showFrame={true}
                    //Show/hide scan frame
                    scanBarcode={true}
                    //Can restrict for the QR Code only
                    laserColor={'green'}
                    //Color can be of your choice
                    frameColor={'green'}
                    //If frame is visible then frame color
                    colorForScannerFrame={'black'}
                    //Scanner Frame color
                    onReadCode={event =>
                        this.onBarcodeScan(event.nativeEvent.codeStringValue)
                    }
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        shipment: state.shipment.shipmentdata,
        loder: state.loder,
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt, create_shipmemnt })(Shipment_Order_ShowMore)