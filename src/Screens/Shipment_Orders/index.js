import React from 'react';
import {
    Button,
    View,
    Text,
    StatusBar,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert,
    BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Header from "../../components/Header"

import { scale, moderateScale, verticalScale } from '../../components/Scale';
import HamburgerIcon from '../../components/HamburgerIcon';
import Shipment_Order_Card from "../../components/Shipment_Order_Card";


class Shipment_Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:
                [
                    {
                        image: require('../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        client: 'Unicef',
                        quantity: '4,00,000',
                    },

                    {
                        image: require('../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        client: 'ABC Hospital',
                        quantity: '3,00,000',
                    },

                    {
                        image: require('../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        client: 'Unicef',
                        quantity: '2,00,000',
                    },
                    {
                        image: require('../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        client: 'Unicef',
                        quantity: '4,00,000',
                    },

                    {
                        image: require('../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        client: 'ABC Hospital',
                        quantity: '3,00,000',
                    },

                    {
                        image: require('../../assets/user.png'),
                        name: "Fionna Jackson",
                        date: "29/03/2014",
                        shipment_id: "SHQ3987A5",
                        client: 'Unicef',
                        quantity: '2,00,000',
                    },
                ],
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Shipment');
        return true;
    }

    _click = (item, index) => {
        console.log('click', item);

        this.props.navigation.navigate('Shipment_Order_ShowMore', { data: item.shipmentId, id: index });
    }
    render() {
        console.log('shipmeny order', this.props.shipment);

        return (
            <View style={{
                flex: 1,
                alignItems: "center"
            }}>
                <StatusBar backgroundColor="#0093E9" />
                <Header name={'Shipment Orders'} />

                <ScrollView
                    nestedScrollEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false} style={{ marginTop: verticalScale(-90) }}>


                    {this.props.shipment.length === 0 ? 
                    <View>
                    <Image style={{ marginTop: verticalScale(100),width: scale(100), height: scale(100),alignSelf:"center" }}
                        resizeMode='cover'
                        source={require('../../assets/shipping.png')} />
                        <Text style={{fontSize:scale(20)}}>Shipment Orders are Empty</Text>
                        </View> :
                        this.props.shipment.map((item, index) => (
                            <>
                                {
                                    item.status === "Shipped" ?
                                        <Shipment_Order_Card button={item} onClick={() => { this._click(item, index) }} />
                                        : null}
                            </>
                        ))
                    }
                    <View style={{ height: scale(20) }} />

                </ScrollView>

            </View>
        )
    }
}

const customiseShipmentData = (shipment) => {

    let data = [];
    for (let i in shipment) {
        const Object = {};
        const total = 0;
        if (shipment[i].status === "Shipped") {
            Object = shipment[i]
            Object['isExpanded'] = false
            data.push(Object);
        }
    }

    console.log('data', data);

    return data;

}

function mapStateToProps(state) {
    return {
        shipment: customiseShipmentData(state.shipment.shipmentdata),
        loder: state.loder,
    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, inventory, fetch_shipmemnt })(Shipment_Orders)