import { SHIPMENT, UPDATE_SHIPMENT } from '../constant'
export default function (state = { shipmentdata: [] }, action) {
    switch (action.type) {
        case SHIPMENT:
            return {
                ...state,
                shipmentdata: action.payload.data.map(shipmentdata => JSON.parse(shipmentdata[0].data)),
            }
        case UPDATE_SHIPMENT:
            console.log('update', action.payload);
            console.log('state', state.shipmentdata);
           // console.log('index',this.state.indexOf(shipmentdata => shipmentdata.shipmentId === action.payload.shipmentId));
           // const shipmentid = this.state.findIndex(shipmentdata => shipmentdata.shipmentId === action.payload.shipmentId)   
            const p = state.shipmentdata.map((item) => {
                if (item.shipmentId === action.payload.shipmentId) {
                    return { ...item, ...action.payload }
                } else {
                    return item
                }
            })
            return{
                ...state,
                shipmentdata: p
            }
        

        default:
            return state
    }
}
