import {INVENTORY,ADD_INVENTORY} from '../constant'
export default function(state={inventorydata:[]}, action){
    switch(action.type){
        case INVENTORY:
            console.log('reducer_inventorydata',action.payload);
            return{
                ...state,
                inventorydata:action.payload.data.map(inventorydata => JSON.parse(inventorydata.data)),
            }
        case ADD_INVENTORY:
            return{
                ...state,
                inventorydata:[...action.payload,...state.inventorydata,]
            }
        default:
            return state
    }
}
