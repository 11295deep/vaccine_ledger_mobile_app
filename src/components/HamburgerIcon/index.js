import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { scale, moderateScale, verticalScale} from '../Scale';
import { TouchableOpacity, View, Text, Image, Dimensions, TouchableHighlight } from 'react-native';

class HamburgerIcon extends Component {
    render() {
        return (
            <TouchableOpacity
                style={{
                    width: scale(16),
                    height: scale(14),
                    
                    justifyContent: "center"
                }}
                onPress={() => {
                    this.props.navigation.toggleDrawer();
                }}>
                <Image
                    style={{ width: scale(16),
                        height: scale(14), borderWidth: 0, }}
                    source={require("../../assets/Menu.png")}
                    resizeMode='contain'
                />
            </TouchableOpacity>
        )
    };
}
export default withNavigation(HamburgerIcon);