import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import { scale, moderateScale, verticalScale } from "../Scale";

export default class Shipment_Card extends Component {
    constructor() {
        super();
        this.state = {
            layoutHeight: 0,
            hide: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.button.isExpanded) {
            this.setState(() => {
                return {
                    layoutHeight: null,
                    hide: true,
                };
            });
        } else {
            this.setState(() => {
                return {
                    layoutHeight: 0,
                    hide: false,
                };
            });
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layoutHeight !== nextState.layoutHeight) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View style={{ marginTop: verticalScale(15), width: scale(328), flex: 1, backgroundColor: "#FFFFFF", borderRadius: 8 }}>
                <View style={{ height: scale(135), position: "relative" }}>
                    <View style={{ flexDirection: "row", height: "70%" }}>

                        <View style={{ width: scale(30), marginTop: verticalScale(10), marginLeft: scale(10), borderRadius: 10, height: scale(30) }}>
                            <Image style={{ width: scale(30), height: scale(30) }}
                                resizeMode='center'
                                source={require('../../assets/user.png')} />
                        </View>

                        <View style={{ width: "50%", marginTop: verticalScale(10), marginLeft: scale(10), flexDirection: "column" }}>
                            <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0093E9", fontWeight: "bold" }}>{this.props.button.deliveryTo}</Text>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Shipment ID</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000" }}>{this.props.button.shipmentId}</Text>
                            </View>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Quantity</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{this.props.button.products[0].quantity}</Text>
                            </View>

                            <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Client Name</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }}>{this.props.button.client}</Text>
                            </View>

                            {this.state.hide ? <View style={{ marginTop: verticalScale(7), flexDirection: "row", }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#707070", width: "60%" }}>Transaction Id</Text>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", color: "#000000", fontWeight: "bold" }} numberOfLines={1}>{this.props.button.receiver}</Text>
                            </View> : null}


                        </View>

                        <View style={{ width: "28%", marginTop: verticalScale(10), marginLeft: scale(10), marginRight: scale(10), flexDirection: "column" }}>
                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#707070", alignSelf: "flex-end" }}>{this.props.button.estimateDeliveryDate}</Text>

                            <View style={{ width: scale(58), height: scale(24), borderRadius: 12, backgroundColor: (this.props.button.status === "In Transit") ? "#FFB100" : "#3CB049", alignSelf: "flex-end", marginTop: verticalScale(18), justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#FFFFFF" }}>{this.props.button.status}</Text>
                            </View>

                        </View>

                    </View>
                    <View style={{ alignItems: "center", marginTop: verticalScale(12) }}>

                        {!this.state.hide ? <View style={{ alignItems: "center" }}>
                            <View style={{ width: scale(308), borderWidth: 1, borderColor: "#F6F6F6", borderRadius: 2, }} />   
                            {/* #F6F6F6 */}
                            <TouchableOpacity activeOpacity={0.5} onPress={this.props.onClickFunction} disabled={this.props.disabled}>
                                <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", marginTop: verticalScale(8.5) }}>SHOW MORE</Text>
                            </TouchableOpacity>
                        </View> : null}

                    </View>

                </View>

                <View style={{ height: this.state.layoutHeight, overflow: 'hidden', }}>
                    <View style={{ width: scale(308), borderWidth: 1, borderColor: "#F6F6F6", borderRadius: 2, alignSelf: "center" }} />
                    {this.props.button.products.map((item, key) => (
                        <View style={{ marginLeft: scale(10), marginTop: verticalScale(10) }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <View style={{ width: scale(25), borderRadius: 400, height: scale(25), backgroundColor: item.color, justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: "#707070", fontSize: scale(7), fontFamily: "Roboto-Bold", fontWeight: "bold" }}>{item.productName}</Text>
                                </View>
                                <Text style={{ fontSize: scale(14), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(13) }}>{item.productName}</Text>
                                <Text style={{ fontSize: scale(9), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(10) }}></Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(5) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13) }}>Manufacturer</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{item.manufacturerName}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13) }}>Qty</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{item.quantity}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13) }}>Mfg Date</Text>
                                {item.manufacturingDate.date ?
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{new Date(item.manufacturingDate.date).toLocaleDateString()}</Text>
                                    : <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{item.manufacturingDate}</Text>
                                }
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13) }}>Exp Date</Text>
                                {item.expiryDate.date1 ?
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{new Date(item.expiryDate.date1).toLocaleDateString()}</Text> :
                                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{item.expiryDate}</Text>
                                }
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13) }}>Batch No</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{item.batchNumber}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: "center", marginLeft: scale(25), marginTop: verticalScale(10) }}>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#707070", marginLeft: scale(13) }}>Serial No.</Text>
                                <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Regular", color: "#000000", marginLeft: scale(10) }}>{item.serialNumber}</Text>
                            </View>
                            <View style={{ height: scale(10), }} />
                        </View>
                    ))}
                    {this.state.hide ? <View style={{ alignItems: "center", height: scale(40) }}>
                        <View style={{ width: scale(308), borderWidth: 1, borderColor: "#F6F6F6", borderRadius: 2, }} />
                        <TouchableOpacity activeOpacity={0.5} onPress={this.props.onClickFunction} disabled={this.props.disabled}>
                            <Text style={{ fontSize: scale(10), fontFamily: "Roboto-Regular", color: "#0093E9", marginTop: verticalScale(8.5) }}>SHOW LESS</Text>
                        </TouchableOpacity>
                    </View> : null}
                </View>

            </View>
        );
    }
}