import React from 'react';
import {
    Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';

class Custom_Side_Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            pass: '',
        }
    }

    _signout = async () => {
        await AsyncStorage.clear();
        alert("logout");
        this.props.navigation.navigate('Auth');
    }
    render() {
        const { user } = this.props;
        return (
            <ScrollView>
                <SafeAreaView
                    style={{ alignItems: "center" }}
                    forceInset={{ top: 'always', horizontal: 'never' }}>

                    <View style={{ marginTop: verticalScale(70), justifyContent: "center", alignItems: "center", width: scale(91), height: scale(91), borderRadius: 400, }}>
                        <Image style={{ width: scale(91), height: scale(91), borderRadius: 400 }}
                            resizeMode='cover'
                            source={{uri: user.profile_picture}} />
                    </View>
                    <Text style={{ fontSize: scale(13), fontFamily: "Roboto-Bold", color: "#0B65C1", fontWeight: "bold", marginTop: verticalScale(7) }}>{user.name}</Text>
                    <Text style={{ fontSize: scale(8), fontFamily: "Roboto-Regular", color: "#0B65C1", marginTop: verticalScale(3) }}>{user.organisation}</Text>
                    <View style={{ width: "100%", marginLeft: scale(36) }}>

                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(47) }}
                            onPress={() => {
                                this.props.navigation.navigate('Profile');
                                this.props.navigation.closeDrawer();
                            }}>
                            <Image
                                style={{ width: scale(32), height: scale(32), }}
                                source={require('../../assets/profile.png')}
                                resizeMode='cover'
                            />
                            <Text
                                style={{ fontSize: scale(16), marginLeft: scale(25), color: "#0093E9" }}

                            >
                                Profile
                    </Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(37) }}
                            onPress={() => {
                                this.props.navigation.closeDrawer();
                            }}>
                            <Image
                                style={{ width: scale(32), height: scale(32), }}
                                source={require('../../assets/Aboutus.png')}
                                resizeMode='cover'
                            />
                            <Text
                                style={{ fontSize: scale(16), marginLeft: scale(25), color: "#0093E9" }}

                            >
                                About Us
                    </Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(37) }}
                            onPress={() => {
                                this._signout();
                                this.props.navigation.closeDrawer();
                            }}>
                            <Image
                                style={{ width: scale(32), height: scale(32), }}
                                source={require('../../assets/logout.png')}
                                resizeMode='cover'
                            />
                            <Text
                                style={{ fontSize: scale(16), marginLeft: scale(25), color: "#0093E9" }}

                            >
                                Logout
                    </Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </ScrollView>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.userinfo.user
    }
}

export default connect(mapStateToProps, {})(Custom_Side_Menu)