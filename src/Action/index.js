import { REGISTER, OTP, LOGIN, USERINFO, USERUPDATE, HIDELODER, SHOWLODER, SHIPMENT,INVENTORY, ADD_INVENTORY,UPDATE_SHIPMENT } from '../constant';
import axios from 'axios';
import url from "../API";
import AsyncStorage from '@react-native-community/async-storage';
import {config} from "../config";
import setAuthToken from "../utils/setAuthToken";

export const saveUser = (userdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: REGISTER,
        payload: userdata
    })
    try {
        const response = await axios.post(config().registerUrl,userdata);
        //const response = await axios.post(`${url}/usermanagement/api/auth/register`, userdata);
        console.log("Action_saveUser_response", response);
        if (response.data.message === "Registration Success.") {
            callback()
            alert('Successfully Register')
        }
        return response;
    }
    catch (e) {
        console.log("Action_saveUser_error", e.response);
        const err = e.response.data.data[0];
        if(err.msg==="E-mail already in use"){
            alert(err.msg);
        }else{
            alert(e);
        }
        return e.response;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const verify_otp = (otpdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: OTP,
        payload: otpdata
    })
    try {
        const response = await axios.post(config().verifyOtpUrl,otpdata);
        //const response = await axios.post(`${url}/usermanagement/api/auth/verify-otp`, otpdata);
        console.log("Action_verify_otp_response", response);
        if (response.data.message === "Account confirmed success.") {
            callback()
            alert('Successfully Verify')
        }
        return response;
    }
    catch (e) {
        console.log("Action_verify_otp_error", e.response);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const resend_otp = (otpdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    // dispatch({
    //     type: OTP,
    //     payload: otpdata
    // })
    try {
        const response = await axios.post(`${url}/usermanagement/api/auth/resend-verify-otp`, otpdata);
        console.log("Action_resend_otp_response", response);
        if (response.data.message === "Confirm otp sent.") {
            alert('Resend OTP Successfully')
        }
        return response;
    }
    catch (e) {
        console.log("Action_resend_otp_error", e.response);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const login = (logindata, rem, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: LOGIN,
        payload: rem
    })

    try {
        const response = await axios.post(config().loginUrl,logindata);
        //const response = await axios.post(`${url}/usermanagement/api/auth/login`, logindata);
        console.log("Action_login_response", response.data.data.token);
        if (response.data.message === "Login Success.") {
            callback()
            if (getState().login.rem) {
                console.log('true', getState().login.rem);
                await AsyncStorage.setItem('logged', '1');
                await AsyncStorage.setItem('token', response.data.data.token);
                setAuthToken(response.data.data.token);
                alert('Successfully Login')

                dispatch({
                    type: LOGIN,
                    payload: response.data.data.token
                })
            } else {
                console.log('false', getState().login.rem);
                setAuthToken(response.data.data.token);
                alert('Successfully Login')
                dispatch({
                    type: LOGIN,
                    payload: response.data.data.token
                })
            }
        }
        return response;
    }
    catch (e) {
        console.log("Action_login_error", e)
        const err = e.response.data.message;
        if(err === "Email or Password wrong."){
            alert('Invalid Username/Password');
        }else{
            alert(e);
        }
        return e.response;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const upload_Image = (image, imageredux, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        const configs = {
            headers: {
                'content-type': 'multipart/form-data',
            },
        };
        response = await axios.post(config().upload,image);
        //response = await axios.post(`${url}/usermanagement/api/auth/upload`, image, configs);
        if (response.data.message === "Updated") {
            alert('Image successfully updated')
            dispatch({
                type: USERUPDATE,
                payload: { profile_picture: imageredux }
            })
        }
        console.log("Action_upload_Image_response", response);
        return response;
    } catch (e) {
        console.log("Action_upload_Image_error", e.response);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const user_update = (data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        response = await axios.post(config().updateProfileUrl,data);
        //response = await axios.post(`${url}/usermanagement/api/auth/updateProfile`, data);
        if (response.status === 200) {
            alert('Successfully updated')
            dispatch({
                type: USERUPDATE,
                payload: data
            })
        }
        console.log("Action_user_update_response", response);
        return response;
    } catch (e) {
        console.log("Action_user_update_error", e.response);
        console.log("Action_user_update_error", e);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const userinfo = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().userInfoUrl);
       // const response = await axios.get(`${url}/usermanagement/api/auth/userInfo`);
        console.log("Action_userinfo_response", response.data);
        dispatch({
            type: USERINFO,
            payload: response.data.data
        })
        return response.data;
    }
    catch (e) {
        console.log("Action_userinfo_error", e.response)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const inventory = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("Action_inventory");
    try {
        const response = await axios.get(config().inventoriesUrl);
       // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
        console.log("Action_inventory _response", response.data);
        if(response.data.data)
        dispatch({
            type: INVENTORY,
            payload: response.data
        })
        return response.data;
    }
    catch (e) {
        console.log("Action_inventory_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const add_inventory = (data, callback = () => { }) => async (dispatch, getState) => {
    try {
        console.log('start');
        const responses = await Promise.all(data.map((item) => {
            return axios.post(config().addInventoryUrl, { data: item });
            //return axios.post(`${url}/inventorymanagement/api/inventory/addNewInventory`, { data: item })
        }))
        //const response = await axios.post(`${url}/inventorymanagement/api/inventory/addNewInventory`,data);
        console.log("Action_add_inventory _response", responses);
        //error alert           
        for (let index in responses) {
            let response = responses[index]
            if (response.status !== 200) {
                //if one status is failed so it is going to the catch
                alert('Failed to Add Inventory')
                throw new Error('inventory unsuccessful')                
            }
        }
        alert('Successfully Add Inventory')

        dispatch({
            type: ADD_INVENTORY,
            payload: data
        })
        callback()
        return responses;
    }
    catch (e) {
        console.log("Action_add_inventory_error", e.response)
        alert(e);
    }
}

export const fetch_shipmemnt = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("Action_inventory");
    try {
        const response = await axios.get(config().shipmentsSearch+'SHP2345'); //SHP9876
       // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
        console.log("Action_fetch_shipmemnt _response", response.data);
        if(response.data.data)
        dispatch({
            type: SHIPMENT,
            payload: response.data
        })
        return response.data;
    }
    catch (e) {
        console.log("Action_fetch_shipmemnt_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const create_shipmemnt = (data,address, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("Action_inventory");
    try {
        const response = await axios.post(config().createShipmentUrl+ address,{data:data});
        console.log("Action_create_shipmemnt_response", response);
        if (response.status === 200) {
            dispatch({
                    type: UPDATE_SHIPMENT,
                    payload: data
                })
        }
        return response;
    }
    catch (e) {
        console.log("Action_create_shipmemnt_error", e.response)
        console.log("Action_create_shipmemnt_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const fetchPublisherShipments = () => async (dispatch, getState) => {
    loadingOn(dispatch)
    console.log("Action_inventory");
    try {
        const response = await axios.get(config().shipmentsUrl); //SHP9876
       // const response = await axios.get(`${url}/inventorymanagement/api/inventory/getAllInventoryDetails`);
        console.log("Action_fetchPublisherShipments_response", response);
        if(response.status === 200)
        dispatch({
            type: SHIPMENT,
            payload: response.data
        })
        return response.data;
    }
    catch (e) {
        console.log("Action_fetchPublisherShipments_error", e.response)
        console.log("Action_fetchPublisherShipments_error", e)
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}



export const loadingOn = (dispatch) => {
    dispatch({
        type: SHOWLODER,
    });
};

export const loadingOff = (dispatch) => {
    dispatch({
        type: HIDELODER,
    });
};

//   export const login=(logindata,rem,callback=()=>{})=> async (dispatch,getState)=>{
//     dispatch({
//         type:LOGIN,
//         payload:rem
//     })
//     if(getState().login.rem){
//         console.log('true',getState().login.rem);
//     }else{
//         console.log('false',getState().login.rem);
//     }
//       try {
//           const response = await axios.post(`${url}/usermanagement/api/auth/login`, logindata);
//           console.log("resp", response);
//           if(response.data.message === "Login Success."){
//               callback()
//               await AsyncStorage.setItem('logged', '1');
//               await AsyncStorage.setItem('token', response.data.data.token);
//               alert('Successfull Login')
//               dispatch({
//                 type:LOGIN,
//                 payload:response.data.data.token
//             })
//               return response;
//           }   
//       }
//       catch(e){
//           alert(e);
//       }    
// }